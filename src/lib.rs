// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

#![cfg_attr(not(test), no_std)]

//! This crate implements the audio modulated matrix LED driver IS31FL3731 by Integrated Silicon
//! Solution Inc. (ISSI) ([Datasheet](https://www.issi.com/WW/pdf/31FL3731.pdf)).
//!
//! To reduce CPU usage, up to 8 frames can be stored with individual time delays between frames
//! to play small animations automatically. LED frames can be modulated with audio signal, but is not implemented in this crate yet.
//! The matrix driver can handle up to 144 separated LEDs which are aligned in two blocks.
//! Each LED can be dimmed individually with 8-bit, resulting in 256 steps of dimming.
//!
//! # License
//!
//! Open Logistics Foundation License\
//! Version 1.3, January 2023
//!
//! See the LICENSE file in the top-level directory.
//!
//! # Contact
//! Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>

use embedded_hal::blocking::i2c;

/// Maximal possible dimension of a matrix
///
/// According to the maximal amount of LEDs (144 = 16 * 9)
const MAX_WIDTH: usize = 16;
const MAX_HEIGHT: usize = 9;

/// Driver-specific quantized delay type
///
/// Several features in the LED matrix driver can be timed. In general, the timing is controlled by
/// setting the value `A` as a few bits in a control register. To calculate the resulting delay,
/// `A` is either multiplied with `TAU` which is the smallest quantization unit (e.g. for playing
/// animations or blinking) or by calculating `TAU*2^A` (e.g. for the breath control feature).
///
/// The desired [`core::time::Duration`] is saturated, i.e. forced on the possible value range. If
/// the requested delay can not be represented exactly, the value is rounded down, so the resulting
/// delay will be shorter than the requested delay. The minimum and maximum delays are:
/// * For the `A*TAU` case: min. `TAU`, max. `A_MAX*TAU`
/// * For the `TAU*2^A` case: min. `TAU`, max. `TAU*2^A_MAX`
///
/// For the "frame delay time" for the autoplay feature, the special case `A=0 => FDT=64*TAU` is
/// _ignored_! So the maximum "frame delay time" is 693ms instead of 704ms. This decision has been
/// made because that exception is not documented for the "blink period time" for the blink
/// feature.
pub struct Delay<const TAU_US: u32, const A_MAX: u8, const EXPONENTIAL: bool> {
    value: u8,
    is_exact: bool,
}

// Custom log2 function
// TODO Replace by https://doc.rust-lang.org/std/primitive.u128.html#method.log2
// as soon as stabilized
fn log2(mut value: u128) -> u8 {
    let mut exponent = 0;
    loop {
        if value <= 1 {
            return exponent;
        }
        value /= 2;
        exponent += 1;
    }
}

impl<const TAU_US: u32, const A_MAX: u8, const EXPONENTIAL: bool>
    Delay<TAU_US, A_MAX, EXPONENTIAL>
{
    pub fn new(delay: core::time::Duration) -> Self {
        let desired_micros = delay.as_micros();

        let desired_value = if !EXPONENTIAL {
            desired_micros / TAU_US as u128
        } else {
            log2(desired_micros / TAU_US as u128) as u128
        };

        if desired_value < 1 && !EXPONENTIAL {
            Self {
                value: 1,
                is_exact: false,
            }
        } else if desired_value > A_MAX as u128 {
            Self {
                value: A_MAX,
                is_exact: false,
            }
        } else {
            let is_exact = if !EXPONENTIAL {
                TAU_US as u128 * desired_value == desired_micros
            } else {
                TAU_US as u128 * (1 << desired_value) == desired_micros
            };
            Self {
                value: desired_value as u8,
                is_exact,
            }
        }
    }

    /// Returns the rounded microseconds
    pub fn micros(&self) -> u32 {
        if !EXPONENTIAL {
            TAU_US * self.value as u32
        } else {
            TAU_US * (1 << self.value as u32)
        }
    }

    /// Returns `A`
    pub fn value(&self) -> u8 {
        self.value
    }

    /// Returns if this quantized delay matches the desired delay exactly or if it was rounded or
    /// saturated
    pub fn is_exact(&self) -> bool {
        self.is_exact
    }
}

/// Command register address
///
/// The Command Register should be configured first after writing in the slave address to
/// choose the available register (Frame Registers and Function Registers). Afterwards the chosen register can be written.
const CMD_REG: u8 = 0xFD;

/// Control Register stores on or off state for each LED (1 bit per LED => 18 Byte)
const FRAME_CONTROL_REG: u8 = 0x00;

/// Blink Register controls the blink function of each LED (1 bit per LED => 18 Byte)
const FRAME_BLINK_REG: u8 = 0x12;

/// PWM Register sets the duty cycle of each individual LED (8 bit per LED => 144 Byte)
const FRAME_PWM_REG: u8 = 0x24;

//const MIN_WAIT_BETWEEN_COMMAND_MS: u32 = 1;

/// Public struct for holding a frame array with the given size
pub struct Frame<const WIDTH: usize, const HEIGHT: usize> {
    pub pixels: [[u8; WIDTH]; HEIGHT],
}

/// Power Pin of the Sensors
///
/// Allows the drivers user (!) to enable/disable the sensor while it is not in use for power saving.
#[derive(PartialEq, Eq)]
pub enum PowerActive<PwrPin: embedded_hal::digital::v2::OutputPin> {
    High(PwrPin),
    Low(PwrPin),
    AlwaysOn,
}

/// Slave I2C addresses is determined by the AD pin of the driver
#[derive(Copy, Clone, PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub enum Addr {
    GND = 0b1110100, // AD pin connected to GND
    VCC = 0b1110111, // AD pin connected to VCC
    SCL = 0b1110101, // AD pin connected to SCL
    SDA = 0b1110110, // AD pin connected to SDA
}

/// Register Definitions for the eight frames
#[derive(Copy, Clone, PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub enum FrameId {
    One = 0x00,
    Two = 0x01,
    Three = 0x02,
    Four = 0x03,
    Five = 0x04,
    Six = 0x05,
    Seven = 0x06,
    Eight = 0x07,
}

/// Number of loops playing selection. Taken from table 10 in the datasheet.
pub enum AnimationLoops {
    Endless = 0x00,
    One = 0x01,
    Two = 0x02,
    Three = 0x03,
    Four = 0x04,
    Five = 0x05,
    Six = 0x06,
    Seven = 0x07,
}

/// Register Definitions of page nine (Function Register). Taken from table 3 in the datasheet.
#[derive(PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub enum Functions {
    Config = 0x00,
    PictureDisplay = 0x01,
    AutoPlayCtrl1 = 0x02,
    AutoPlayCtrl2 = 0x03,
    DisplayOption = 0x05,
    AutoSynch = 0x06,
    FrameState = 0x07,
    BreathCtrl1 = 0x08,
    BreathCtrl2 = 0x09,
    Shutdown = 0x0A,
    AGCCtrl = 0x0B,
    AudioADCRate = 0x0C,
}

/// Register Definitions of the Configuration. Taken from Table 8 in the datasheet.
#[derive(PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub enum Mode {
    Picture = 0x00,
    AutoFramePlay = 0x01,
    AudioFramePlay = 0x02,
}

#[derive(PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub enum Page {
    Frame(FrameId),
    Function,
}

impl Page {
    fn id(&self) -> u8 {
        match self {
            Page::Frame(id) => *id as u8,
            Page::Function => 0x0B, // Definition of the Function Register.
        }
    }
}

/// Register Definitions of the Shutdown. Taken from table 17 in the datasheet.
#[derive(PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
enum ShutdownControl {
    ShutdownMode = 0x00,
    NormalOperation = 0x01,
}

/// Custom type for enabling or disabling the Blink functionality.
#[derive(Clone, Copy, PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub enum Blink {
    Enable = 0x01,
    Disable = 0x00,
}

/// Custom type for setting the IntensityControl of the Blink functionality.
///
/// - `LikeFirstFrame` sets the intensity of each frame like the one of the first frame.
/// - `Individual` takes the intensity of each frame independently.
#[derive(PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub enum IntensityControl {
    LikeFirstFrame = 0x01,
    Individual = 0x00,
}

/// Custom type for enabling or disabling the Breath functionality.
#[derive(PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub enum Breath {
    Enable = 0x01,
    Disable = 0x00,
}

/// Custom type for handling the matrix orientation.
#[derive(Copy, Clone, PartialEq, PartialOrd, Eq, Ord, Debug, Hash)]
pub enum Orientation {
    SensorUp,
    SensorDown,
}

/// LED driver type
///
/// Stores the I2C address and the device peripherals.
pub struct LedMatrix<PwrPin, ShutdownPin, I2C, const WIDTH: usize, const HEIGHT: usize>
where
    PwrPin: embedded_hal::digital::v2::OutputPin,
    ShutdownPin: embedded_hal::digital::v2::OutputPin,
    I2C: i2c::WriteRead + i2c::Write,
{
    addr: Addr,
    power_active: PowerActive<PwrPin>,
    shutdown_pin: ShutdownPin,
    i2c: I2C,
}

#[derive(Debug)]
pub enum Error<PwrPinErr, ShutdownPinErr, I2cErr> {
    PwrPin(PwrPinErr),
    ShutdownPin(ShutdownPinErr),
    I2c(I2cErr),
}

// Helper macro so we do not have to type out the whole generic error type for each method
macro_rules! GenericError {
    () => {
        Error<PwrPin::Error, ShutdownPin::Error, I2cErr>
    };
}

impl<PwrPin, ShutdownPin, I2C, I2cErr, const WIDTH: usize, const HEIGHT: usize>
    LedMatrix<PwrPin, ShutdownPin, I2C, WIDTH, HEIGHT>
where
    PwrPin: embedded_hal::digital::v2::OutputPin,
    ShutdownPin: embedded_hal::digital::v2::OutputPin,
    I2C: i2c::WriteRead<Error = I2cErr> + i2c::Write<Error = I2cErr>,
{
    /// Create a new instance of the LED driver
    pub fn new(
        addr: Addr,
        power_active: PowerActive<PwrPin>,
        shutdown_pin: ShutdownPin,
        i2c: I2C,
    ) -> Self {
        Self {
            addr,
            power_active,
            shutdown_pin,
            i2c,
        }
    }

    /// Select the Function Page and write a `value` to the register `reg`
    fn write_function_register(
        &mut self,
        reg: Functions,
        value: u8,
    ) -> Result<(), GenericError!()> {
        self.select_page(Page::Function)?;
        self.i2c
            .write(self.addr as u8, &[reg as u8, value])
            .map_err(Error::I2c)
    }

    /// Writes the given `page` into the Command register, i.e. selects the given `page`
    fn select_page(&mut self, page: Page) -> Result<(), GenericError!()> {
        self.i2c
            .write(self.addr as u8, &[CMD_REG, page.id()])
            .map_err(Error::I2c)
    }

    /// Writes the Shutdown register
    fn set_shutdown(&mut self, shutdown: ShutdownControl) -> Result<(), GenericError!()> {
        self.write_function_register(Functions::Shutdown, shutdown as u8)
    }

    /// Writes to the Configuration register to set the `mode` and the start frame for the
    /// AutoPlayAnimation (only needed for AutoPlay functionality)
    fn set_configuration(
        &mut self,
        mode: Mode,
        start_frame_autoplay: FrameId,
    ) -> Result<(), GenericError!()> {
        let value = (mode as u8) << 3 | start_frame_autoplay as u8;
        self.write_function_register(Functions::Config, value)
    }

    /// Selects the PictureDisplay Mode and writes the `picture` frame
    fn set_picture_display(&mut self, picture: FrameId) -> Result<(), GenericError!()> {
        self.write_function_register(Functions::PictureDisplay, picture as u8)
    }

    // Reads the FrameStateRegister with Interrupt Bit of the Animation and current FrameId
    fn write_read_register(&mut self, reg: Functions) -> Result<u8, GenericError!()> {
        let mut buf = [0_u8; 1];
        // self.i2c.write(self.addr as u8, &[reg as u8]).map(|_|());
        // self.delay.delay_ms(10);
        // self.i2c.read(self.addr as u8, &mut [buff]).map(|_|());
        // self.delay.delay_ms(10);
        self.i2c
            .write_read(self.addr as u8, &[reg as u8], &mut buf)
            .map_err(Error::I2c)?;

        Ok(buf[0])
    }

    /// Power the driver on (true) or off (false)
    ///
    /// This method sets the power pin according to the bool which is given, but stays in software
    /// "ShutdownMode". To enable the LEDs, the chip needs to be in "NormalOperation", which should
    /// be set with the [`show_frame()`](LedMatrix::show_frame) method after writing to the desired
    /// frame.
    pub fn power_on(&mut self, on: bool) -> Result<(), GenericError!()> {
        if !on {
            self.set_shutdown(ShutdownControl::ShutdownMode)?;
            self.shutdown_pin.set_low().map_err(Error::ShutdownPin)?;
        }
        match (&mut self.power_active, on) {
            (PowerActive::High(pp), true) => pp.set_high(),
            (PowerActive::Low(pp), true) => pp.set_low(),
            (PowerActive::High(pp), false) => pp.set_low(),
            (PowerActive::Low(pp), false) => pp.set_high(),
            (_, _) => Ok(()),
        }
        .map_err(Error::PwrPin)?;
        if on {
            self.shutdown_pin.set_high().map_err(Error::ShutdownPin)?;
            self.set_shutdown(ShutdownControl::ShutdownMode)?;
        }
        Ok(())
    }

    /// Writes an array of PWM values to the desired frame
    ///
    /// The `frame` will be selected through the `frame_id`. The driver will set each control and
    /// PWM pin according to the data in the `frame` array. For used LEDs, the control pin will be
    /// set to one and the PWM values will be set to the data in the `frame` array.
    pub fn write_frame(
        &mut self,
        frame_id: FrameId,
        frame: Frame<WIDTH, HEIGHT>,
    ) -> Result<(), GenericError!()> {
        let mut led_control = [0_u8; 1 + (MAX_WIDTH * MAX_HEIGHT) / 8];
        let mut led_blink = [0_u8; 1 + (MAX_WIDTH * MAX_HEIGHT) / 8];
        let mut led_pwm = [0_u8; 1 + (MAX_WIDTH * MAX_HEIGHT)];

        // First byte of each register is the address
        led_control[0] = FRAME_CONTROL_REG;
        led_blink[0] = FRAME_BLINK_REG;
        led_pwm[0] = FRAME_PWM_REG;

        let mut counter = 0;
        let mut control = 0;

        for y in 0..MAX_HEIGHT {
            for x in 0..MAX_WIDTH {
                if x < WIDTH && y < HEIGHT {
                    led_pwm[1 + y * MAX_WIDTH + x] = frame.pixels[y][x];
                    control <<= 1;
                    control |= 1;
                }

                counter += 1;

                if counter % 8 == 0 {
                    led_control[counter / 8] = control;
                    control = 0;
                }
            }
        }

        self.select_page(Page::Frame(frame_id))?;
        self.i2c
            .write(self.addr as u8, &led_control)
            .map_err(Error::I2c)?;
        self.i2c
            .write(self.addr as u8, &led_blink)
            .map_err(Error::I2c)?;
        self.i2c
            .write(self.addr as u8, &led_pwm)
            .map_err(Error::I2c)?;
        Ok(())
    }

    /// Sets the driver to PictureMode and displays the given frame with `frame_id` on the LED matrix
    pub fn show_frame(&mut self, frame_id: FrameId) -> Result<(), GenericError!()> {
        self.set_configuration(Mode::Picture, FrameId::One)?; // ['FrameId'] irrelevant for PictureMode
        self.set_picture_display(frame_id)?;
        self.set_shutdown(ShutdownControl::NormalOperation)
    }

    /// Clears a frame, i.e. displays a zero array to the given `frame_id`
    pub fn clear_frame(&mut self, frame_id: FrameId) -> Result<(), GenericError!()> {
        let frame_zero = [[0_u8; WIDTH]; HEIGHT];
        self.write_frame(frame_id, Frame { pixels: frame_zero })?;
        self.show_frame(frame_id)
    }

    /// This method will read the FrameStateRegister of the LED driver. The Interrupt Bit (D4)
    /// states if an Animation has finished or not. The Current Frame Display (D2:D0) stores the
    /// currently selected frame.
    pub fn read_frame_state(&mut self) -> Result<u8, GenericError!()> {
        self.write_read_register(Functions::FrameState)
    }

    /// This method starts the AutoPlayAnimation feature
    ///
    /// The AutoPlayAnimation cycles from the `first_frame` to the `last_frame` with the
    /// `frame_delay_time` between each frame.  The `frame_delay_time will be rounded to a
    /// multiple of 11ms with a maximum delay of 693ms.  The amount of animation loops
    /// is set by `repeats`. The type `AnimationLoops` occupies a range from `One` to
    /// `Eight` and `Endless`.
    pub fn play_animation(
        &mut self,
        first_frame: FrameId,
        last_frame: FrameId,
        repeats: AnimationLoops,
        inter_frame_delay: Delay<11000, 63, false>,
    ) -> Result<(), GenericError!()> {
        self.set_configuration(Mode::AutoFramePlay, first_frame)?;

        let mut num_of_frames_playing = last_frame as u8 - first_frame as u8 + 1;
        // FNS 3 bit register: 0 => all frames, 1..7 => 1..7 frames playing
        if num_of_frames_playing > 7 {
            num_of_frames_playing = 0;
        }

        let auto_play_ctrl1 = (repeats as u8) << 4 | num_of_frames_playing;
        self.write_function_register(Functions::AutoPlayCtrl1, auto_play_ctrl1)?;

        let auto_play_ctrl2 = inter_frame_delay.value();
        self.write_function_register(Functions::AutoPlayCtrl2, auto_play_ctrl2)?;
        self.set_shutdown(ShutdownControl::NormalOperation)
    }

    /// Enables/Disables the Blink feature of the LED driver
    ///
    /// If `blink_enable` is set to `Enable` the led driver will blink the selected frame
    /// `frame_id` with a duty cycle of 50% and a delay of `blink_period`. The `blink_period`
    /// will be rounded to a multiple of 270ms with a maximum period of 1890ms.  The
    /// `intensity_control` defines if the intensity equals Frame::One (LikeFirstFrame) or is
    /// set for each frame individually (Individual).
    pub fn blink_frame(
        &mut self,
        frame_id: FrameId,
        blink_enable: Blink,
        intensity_control: IntensityControl,
        blink_period_time: Delay<270000, 7, false>,
    ) -> Result<(), GenericError!()> {
        let display_option =
            (blink_enable as u8) << 3 | (intensity_control as u8) << 5 | blink_period_time.value();
        self.select_page(Page::Frame(frame_id))?;
        self.write_function_register(Functions::DisplayOption, display_option)?;

        if blink_enable == Blink::Enable {
            self.show_frame(frame_id)?;
        }
        Ok(())
    }

    /// Enables/Disables the Breath feature of the LED driver
    ///
    /// If `breath_enable` is set to `Enable` all LEDs will be fade in and out with the given
    /// fade and extinguish times.  This feature is available in PictureMode as well as
    /// AutoPlayAnimation. `fade_in_time` and `fade_out_time` will be rounded to a multiple of
    /// 26ms each with a maximum duration of 182ms. `extinguish_time` will be rounded to a
    /// multiple of 3.5ms with a maximum duration of 24.5ms.
    pub fn breath_control(
        &mut self,
        breath_enable: Breath,
        fade_in_time: Delay<26000, 7, true>,
        fade_out_time: Delay<26000, 7, true>,
        extinguish_time: Delay<3500, 7, true>,
    ) -> Result<(), GenericError!()> {
        let breath_ctrl1 = fade_out_time.value() << 4 | fade_in_time.value();
        let breath_ctrl2 = (breath_enable as u8) << 4 | extinguish_time.value();
        self.write_function_register(Functions::BreathCtrl1, breath_ctrl1)?;
        self.write_function_register(Functions::BreathCtrl2, breath_ctrl2)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use core::time::Duration;

    #[test]
    fn test_log2() {
        assert_eq!(log2(0), 0);
        assert_eq!(log2(1), 0);
        assert_eq!(log2(2), 1);
        assert_eq!(log2(3), 1);
        assert_eq!(log2(4), 2);
        assert_eq!(log2(5), 2);
        assert_eq!(log2(7), 2);
        assert_eq!(log2(8), 3);
        assert_eq!(log2(128), 7);
        assert_eq!(log2(255), 7);
        assert_eq!(log2(256), 8);
        assert_eq!(log2(0xffffffff_ffffffff_ffffffff_ffffffff), 127);
    }

    #[test]
    fn test_quantized_delay() {
        let d = Delay::<1000, 10, false>::new(Duration::from_millis(1));
        assert_eq!(d.value(), 1);
        assert_eq!(d.is_exact(), true);
        let d = Delay::<400, 10, false>::new(Duration::from_millis(1));
        assert_eq!(d.value(), 2);
        assert_eq!(d.is_exact(), false); // rounded
        let d = Delay::<1000, 10, false>::new(Duration::from_millis(0));
        assert_eq!(d.value(), 1);
        assert_eq!(d.is_exact(), false); // desired too low
        let d = Delay::<1000, 10, false>::new(Duration::from_millis(100));
        assert_eq!(d.value(), 10);
        assert_eq!(d.is_exact(), false); // desired too high
        let d = Delay::<1000, 10, true>::new(Duration::from_millis(1));
        assert_eq!(d.value(), 0);
        assert_eq!(d.is_exact(), true);
        let d = Delay::<1000, 10, true>::new(Duration::from_millis(16));
        assert_eq!(d.value(), 4);
        assert_eq!(d.is_exact(), true);
        let d = Delay::<1000, 20, true>::new(Duration::from_millis(17));
        assert_eq!(d.value(), 4);
        assert_eq!(d.is_exact(), false); // rounded
        let d = Delay::<1000, 20, false>::new(Duration::from_millis(17));
        assert_eq!(d.value(), 17);
        assert_eq!(d.is_exact(), true); // with linear it is possible
        let d = Delay::<1000, 10, true>::new(Duration::from_micros(500));
        assert_eq!(d.value(), 0);
        assert_eq!(d.is_exact(), false); // too low
        let d = Delay::<1000, 10, true>::new(Duration::from_millis(1025));
        assert_eq!(d.value(), 10);
        assert_eq!(d.is_exact(), false); // rounded
    }
}

# issi-is31fl3731

This crate implements the audio modulated matrix LED driver IS31FL3731 by Integrated Silicon
Solution Inc. (ISSI) ([Datasheet](https://www.issi.com/WW/pdf/31FL3731.pdf)).

To reduce CPU usage, up to 8 frames can be stored with individual time delays between frames
to play small animations automatically. LED frames can be modulated with audio signal, but is not implemented in this crate yet.
The matrix driver can handle up to 144 separated LEDs which are aligned in two blocks.
Each LED can be dimmed individually with 8-bit, resulting in 256 steps of dimming.

## License

Open Logistics Foundation License\
Version 1.3, January 2023

See the LICENSE file in the top-level directory.

## Contact
Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>
